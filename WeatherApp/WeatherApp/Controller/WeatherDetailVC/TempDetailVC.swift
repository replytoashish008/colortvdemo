//
//  WeatherDetailVC.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherDetailVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet private weak var lbllastUpdated:UILabel!
    @IBOutlet private weak var lblPlaceName:UILabel!
    @IBOutlet private weak var lblTemperature:UILabel!
    @IBOutlet private weak var lblCloudType:UILabel!
    @IBOutlet private weak var lblWind:UILabel!
    @IBOutlet private weak var lblPreci:UILabel!
    @IBOutlet private weak var lblPressure:UILabel!
    @IBOutlet private weak var imgCloud:UIImageView!
    @IBOutlet private weak var vew:UIView!
    @IBOutlet private weak var lblNoRecordFound:UILabel!
    
    // MARK:- Private Variable
    private let viewModel = WeatherDetailViewModel()
    private var activityIndi:UIView? {
        didSet {
            oldValue?.removeFromSuperview()
        }
    }
    
    // MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        apiForDetail()
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Methods
    
    func set(coordinate:CLLocationCoordinate2D) {
        viewModel.coordinate = coordinate
    }
    
    private func initlizeView() {
        if let weatherDetail = viewModel.weatherDetail {
            vew.isHidden = false
            if weatherDetail.localtimeEpoch > 0 {
                lbllastUpdated.text = "\(Date.init(timeIntervalSince1970: TimeInterval(weatherDetail.localtimeEpoch)))"
            }
            lblPlaceName.text = weatherDetail.locationName
            imgCloud.setAssetImage(string: viewModel.weatherDetail?.weatherImgUrl, placeholder: "placeHolder")
            lblCloudType.text = weatherDetail.weatherDesc
            lblTemperature.text = "\(weatherDetail.temp)° C"
            lblWind.text = "\(weatherDetail.wind) kmph"
            lblPreci.text = "\(weatherDetail.percip ?? 0.0) mm"
            lblPressure.text = "\(weatherDetail.pressure) mb"
        } else {
            lblNoRecordFound.isHidden = false
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// API
extension WeatherDetailVC {
    
    private func apiForDetail() {
        activityIndi = createActivityIndicator(view: self.view)
        viewModel.apiForWeatherDetail {[weak self] (error) in
            removeActivityIndicatorFromSuperView(activity: self?.activityIndi)
            if let error = error {
                if self?.viewModel.weatherDetail == nil {
                    showAlertMessage(string: error, viewController: nil)
                }
            }
            DispatchQueue.main.async {
                self?.initlizeView()
            }
        }
    }
}
