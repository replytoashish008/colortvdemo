//
//  WeatherDetailViewModel.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class WeatherDetailViewModel: NSObject {

    struct Request {
        static let accessKey = "access_key"
        static let query = "query"
    }
    var coordinate:CLLocationCoordinate2D!
    var weatherDetail:WeatherDetail?
    
    // api
    func apiForWeatherDetail(completion: @escaping (_ error: String?) -> Void) {
        let operation = NetworkOperations.init(tag: ExtendedUrl.WeatherRelated.detail)
        operation.data_request(urlParameter:[Request.accessKey:accessKey,Request.query:"\(coordinate.latitude),\(coordinate.longitude)"]) { [weak self] (response, error, tag, statusCode) in
            if let err = error, let error = self?.getErrorMessage(error: err) {
                if let weakS = self {
                    if let weatherDetail = WeatherDetail.checkIfCategoryIsAlreadyThere(lat: "\(weakS.coordinate.latitude)", long: "\(weakS.coordinate.longitude)") {
                        self?.weatherDetail = weatherDetail
                    }
                }
                completion(error)
            } else {
                if let response = response as? [String:Any] {
                    if let weakS = self {
                        if let weatherDetail = WeatherDetail.insertNew(data: response) {
                            self?.weatherDetail = weatherDetail
                            completion(nil)
                        } else if let weatherDetail = WeatherDetail.checkIfCategoryIsAlreadyThere(lat: "\(weakS.coordinate.latitude)", long: "\(weakS.coordinate.longitude)") {
                            self?.weatherDetail = weatherDetail
                            completion(nil)
                        } else {
                            completion("No Record found.")
                        }
                    }
                }
            }
        }
    }
        
    private func getErrorMessage(error:Error) -> String? {
        if let error = error as? NetworkError {
            return error.errorMessage()
        } else {
            if (error as NSError).code == NSURLErrorCancelled {
                return nil
            }
            return error.localizedDescription
        }
    }
}
