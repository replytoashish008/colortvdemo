//
//  MapVC.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {
    
    struct Segue {
        static let weatherDetailVC = "SegueWeatherDetailVC"
    }
    
    //MARK:- IBOutlet
    @IBOutlet private weak var mpVew:MKMapView!
    
    //MARK:- Variable
    private let location = LocationRelated.sharedInstance
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        doubleTapGesture()
        btnClickOnCurrentLocation()
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Private Variable
    private func addCurrentLocationAnnotation() {
        
    }
    
    private func doubleTapGesture() {
        let doubleTap = UITapGestureRecognizer.init(target: self, action: #selector(didDoubleTapMap(gesture:)))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.numberOfTouchesRequired = 1
        doubleTap.delegate = self
        mpVew.addGestureRecognizer(doubleTap)
    }
    
    @objc private func didDoubleTapMap(gesture:UIGestureRecognizer) {
        let position = gesture.location(in: mpVew)
        let coordinate = mpVew.convert(position, toCoordinateFrom:  self.mpVew)
        self.performSegue(withIdentifier: Segue.weatherDetailVC, sender: coordinate)
    }
    
    private func moveToMap(to coordinate:CLLocationCoordinate2D) {
        let viewRegion = MKCoordinateRegion(center: coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
        mpVew.setRegion(viewRegion, animated: false)
    }

    // MARK:- IBAction
    
    @IBAction func btnClickOnCurrentLocation() {
        location.starLocation(locationHandler: { [weak self](coordinate) in
            if let coordinate = coordinate {
                self?.moveToMap(to: coordinate)
            }
        }) { (error) in
            showAlertMessage(string: error, viewController: nil)
        }
    }
    
    // MARK: Navigation
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MapVC.Segue.weatherDetailVC {
            let vc = segue.destination as! WeatherDetailVC
            vc.set(coordinate: sender as! CLLocationCoordinate2D)
        }
    }
       
}

extension MapVC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

