//
//  ExtendUrl.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2019 Ask-Fast. All rights reserved.
//

import UIKit

// MARK:- ExtendedUrl
struct ExtendedUrl {

    // MARK:- Authentication
    enum WeatherRelated: NetworkUrl {
        case detail
        
        // MARK: - NetworkUrl
        var methodType: APIHttpMethod {
            get {
                switch self {
                case .detail:
                    return .get
                }
            }
        }
        var extendedUrl: String {
            get {
                switch self {
                case .detail:
                    return ""
                }
            }
        }
        
        var apiContentType: APIContentType {
            get {
                return .json
            }
        }
    }

}
