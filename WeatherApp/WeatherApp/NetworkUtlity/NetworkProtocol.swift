//
//  NetworkProtocol.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2019 CRMApp. All rights reserved.
//

import UIKit

// MARK:- ParameterEncoder
protocol ParameterEncoder {
    static func encode(parameters: Parameters?) throws -> Data?
}

// MARK:- URL String From Paramters
protocol StringFromHttpParamters {
    static func stringFromHttpParameters(data:Parameters?) -> String?
}

// MARK:- NetworkRouter
protocol NetworkRouter: class {
    // , arrPar arrBodyParameter:[Any]?
    func data_request(urlParameter:Parameters?, parameter bodyParameter:Any?, handler:@escaping handler )
    func cancelTask()
}

// MARK:- NetworkErrorProtocol
protocol NetworkErrorProtocol {
    func errorMessage() -> String?
}

// MARK:- NetworkUrl
protocol NetworkUrl {
    var methodType:APIHttpMethod {get}
    var extendedUrl:String {get}
    var apiContentType: APIContentType {get}
}

extension NetworkUrl {
    var getCommonHeader:[String:String] {
        get {
            var header:[String:String] = [:]
            switch apiContentType {
            case .formData:
                header[NetworkOperationsKeys.ApiCall.contentType] = apiContentType.rawValue + "; boundary=\(boundaryString)"
            default:
                header[NetworkOperationsKeys.ApiCall.contentType] = apiContentType.rawValue
            }
            return header
        }
    }
}

