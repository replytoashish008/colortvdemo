//
//  ImageNID.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2019 Ask-Fast. All rights reserved.
//

import UIKit
import AVFoundation

extension UIImageView {
    func setAssetImage(string:String?, placeholder:String?) {
        if let placeholder = placeholder {
            self.image = UIImage.init(named: placeholder)
        }
        NetworkImageDownload().getDownloadImageUrl(url: string) {[weak self] (image) in
            DispatchQueue.main.async {
                if let image = image {
                    self?.image = image
                } else {
                    if let placeholder = placeholder {
                        self?.image = UIImage.init(named: placeholder)
                    }
                }
            }
        }
    }
}

class NetworkImageDownload: Operation {
    
    // MARK:- private static
    private static let shared = NetworkImageDownload()
    private static let imageCache = NSCache<AnyObject, AnyObject>()
    
    // MARK:- init
    override init() {
        NotificationCenter.default.addObserver(forName: UIApplication.didReceiveMemoryWarningNotification, object: nil, queue: nil) { (notification) in
            NetworkImageDownload.imageCache.removeAllObjects()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- Method
    func getDownloadImageUrl(url:String? , image: @escaping (_ image:UIImage?) -> Void) {
        if let imageFromCache = NetworkImageDownload.imageCache.object(forKey: url as AnyObject) as? UIImage {
            image(imageFromCache)
            return
        }
        
        getImageWith(url) {(img, error) in
            if let img = img{
                NetworkImageDownload.imageCache.setObject(img, forKey: url as AnyObject)
                image(img)
            } else {
                image(nil)
            }
        }
    }
    
    // MARK:- Private Methods
    private func getImageWith(_ string: String?,  handler: @escaping ((_ image: UIImage?, Error?) -> Void)) {
        if let string = string, let url = URL.init(string: string) {
            DispatchQueue.global().async {
                do {
                    let data = try Data(contentsOf: url)
                    let thumbImage = UIImage.init(data: data)
                    handler(thumbImage, nil)
                } catch {
                    handler(nil, error)
                }
            }
        }
    }
}
