//
//  JSONParameterEncoding.swift
//  Askfast
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2019 Ask-fast. All rights reserved.
//

import UIKit

struct JSONParameterEncoder: ParameterEncoder {
    static func encode(parameters: Parameters?) throws -> Data? {
        do {
            if let parameters = parameters, parameters.keys.count > 0 {
                let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: [])
                return jsonAsData
            }
            return nil
        }catch {
            throw NetworkError.encodingFailed
        }
    }
    
    static func encode(parameters: [Any]?) throws -> Data? {
        do {
            if let count = parameters?.count, count > 0 {
                let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: [])
                return jsonAsData
            }
            return nil
        }catch {
            throw NetworkError.encodingFailed
        }
    }
}
