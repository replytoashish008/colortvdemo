//
//  NetworkConnection.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2016 Ashish Agrawal. All rights reserved.
//

import Foundation
import UIKit

let baseServerUrl = "http://api.weatherstack.com/current"
let boundaryString = "Boundary-\(NSUUID().uuidString)"
let accessKey = "4e18ff30bf76f6fe3afe763e3d71cb04"

class NetworkOperations<T:NetworkUrl>: Operation, NetworkRouter {
    
    // MARK:- variable
    private var baseUrl = baseServerUrl

    // MARK:- init
    private var tag:T
    private var handlers:handler?
    private var task:URLSessionDataTask?

    // MARK:- init
    init(tag:T) {
        self.tag = tag
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- NetworkRouter Method
    // , arrPar arrBodyParameter:[Any]? = nil
    func data_request(urlParameter:Parameters? = nil, parameter bodyParameter:Any? = nil, handler:@escaping handler ) {
        // used when using singleton class such as refresh token. to avoid multiple hit.
        guard task?.state != .running else {
            return
        }
        // check internet connection error

        if !isInternetAvailable() {
            handler(nil,NetworkError.networkNotAvailable,tag.extendedUrl,0)
            return
        }
        
        // when it is call for the first time, after refresh it will hit again
        if self.handlers == nil {
            handlers = handler
        }
        
        // to check if the value should be added in parameter or not
        
        let urlEncding = URLParameterEncoder.stringFromHttpParameters(data: urlParameter)
        var urlString =  baseUrl + tag.extendedUrl
        if let urlEncding = urlEncding {
            urlString = urlString + "?" + urlEncding
        }
        
        if let url:URL = URL(string:urlString) {
             let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = tag.methodType.rawString
            request.allHTTPHeaderFields = tag.getCommonHeader
            request.httpBody = getHttpData(parameter: bodyParameter)
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            task = session.dataTask(with: request as URLRequest) {
                (data, response, error) in
                self.handleResponse(data: data, response: response, error: error)
            }
            task?.resume()
        } else {
            handler(nil,NetworkError.missingURL,tag.extendedUrl,0)
        }
    }
    
    func cancelTask() {
        task?.cancel()
    }

    func isRunning() -> Bool {
        switch task?.state {
        case .running?:
            return true
        default:
            return false
        }
    }
    
    // MARK:- Handling response
    
    private func handleResponse(data: Data?, response: URLResponse?, error:Error?) {
        responseManagement(data: data, response: response, error: error)
    }

    private func responseManagement(data:Data?, response:URLResponse?, error:Error?) {
        let httpResponse = response as? HTTPURLResponse
        guard let data = data as Data?, let _:URLResponse = response, error == nil else {
            print("error \(error)")
            handlers?(nil,error,self.tag.extendedUrl,httpResponse?.statusCode)
            return
        }
        
        let parseJson1 = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
        print(parseJson1)

        do {
            let parseJson = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            if let json = parseJson as? [String:Any] {
                if let errData = json[NetworkOperationsKeys.Response.error] as? [String:Any] {
                    if let code = errData[NetworkOperationsKeys.Response.code] as? Int {
                            reponseType(nil,NetworkError.serverGenerated(message: errData[NetworkOperationsKeys.Response.info] as? String, code: errData[NetworkOperationsKeys.Response.code] as? Int),self.tag.extendedUrl,httpResponse?.statusCode)
                    } else {
                        reponseType(nil,NetworkError.serverGenerated(message: errData[NetworkOperationsKeys.Response.info] as? String, code: errData[NetworkOperationsKeys.Response.code] as? Int),self.tag.extendedUrl,httpResponse?.statusCode)
                    }
                } else {
                    reponseType(json,nil,self.tag.extendedUrl,httpResponse?.statusCode)
                }
            } else {
                reponseType(parseJson,nil,self.tag.extendedUrl,httpResponse?.statusCode)
            }
        }catch {
            let dataString = String(data: data, encoding: String.Encoding.utf8)
            let error = error as NSError
            reponseType(nil,NetworkError.serverGenerated(message: "Server error", code: error.code),self.tag.extendedUrl,httpResponse?.statusCode)
        }
    }
    
    private func reponseType(_ response:Any? , _ error:Error?,_ tag:String? , _ statusCode:Int?) {
        handlers?(response, error, tag,statusCode)
    }
    
    // MARK:- Private REQUEST Methods
    private func getHttpData(parameter:Any?) -> Data? {
        switch tag.apiContentType {
        case .json:
            if let para = parameter as? Parameters {
                let json = try? JSONParameterEncoder.encode(parameters: para)
                return json
            } else if let para = parameter as? [Any] {
                let json = try? JSONParameterEncoder.encode(parameters: para)
                return json
            }  else {
                return nil
            }
        case .x_www_form_urlencoded:
            if let para = parameter as? Parameters {
                return try? URLParameterEncoder.encode(parameters: para)
            }   else {
                return nil
            }
        default:
            if let para = parameter as? Parameters {
                let json = try? JSONParameterEncoder.encode(parameters: para)
                return json
            } else if let para = parameter as? [Any] {
                let json = try? JSONParameterEncoder.encode(parameters: para)
                return json
            }  else {
                return nil
            }
        }
        
    }
}
