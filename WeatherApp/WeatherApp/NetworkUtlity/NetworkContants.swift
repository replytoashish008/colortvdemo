//
//  APIContentType.swift
//  Askfast
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2019 Ask-fast. All rights reserved.
//

import UIKit

struct NetworkOperationsKeys {
    struct Response {
        static let response = "response"
        static let error = "error"
        static let info = "info"
        static let code = "code"
    }
    
    struct ApiCall {
        static let apiKey = "access_key"
        static let contentType = "Content-Type"
    }
}

typealias HTTPHeaders = [String:String]
typealias Parameters = [String:Any]
typealias handler = (_ response:Any? , _ error:Error?,_ tag:String? , _ statusCode:Int?)->Void

