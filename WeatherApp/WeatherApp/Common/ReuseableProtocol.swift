//
//  ReuseableProtocol.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit

protocol ReuseableProtocol {
    static var reusableIdentifier:String {
        get
    }
}

extension ReuseableProtocol {
    static var reusableIdentifier:String {
        get {
            return "\(self)"
        }
    }
}

