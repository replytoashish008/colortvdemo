//
//  CommonMethods.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit

func showAlertMessage(string:String? , viewController : UIViewController?) {
    if let string = string, string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0 {
        let alert = UIAlertController(title: "", message: string, preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "OK", style: .default , handler: nil)
        alert.addAction(dismiss)
        
        if let viewController = viewController {
            DispatchQueue.main.async {
                viewController.present(alert, animated: true, completion: nil)
            }
        } else {
            DispatchQueue.main.async {
                showAlertController(alert:alert)
            }
        }
    }
}

func showAlertController(alert:UIAlertController) {
    if let nav = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController , let viewController = nav.visibleViewController {
        if Thread.isMainThread {
                viewController.present(alert, animated: true, completion: nil)
           } else {
               DispatchQueue.main.async {
                    viewController.present(alert, animated: true, completion: nil)
               }
           }
    }
}

//MARK:- ActivityIndicator
func createActivityIndicator(view:UIView? ) -> UIView?{
    
    if let view = view {
        let actityIndc = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        actityIndc.backgroundColor = UIColor.black
        actityIndc.alpha = 0.8
        actityIndc.hidesWhenStopped = true
        actityIndc.layer.cornerRadius  = 10
        actityIndc.startAnimating()
        
        let activityView = UIView()
        //    if disableTouch == true {
        var frame = view.frame
        frame.origin.y = 0
        activityView.frame = frame
        actityIndc.frame = CGRect.init(x: view.frame.size.width/2 - 45, y: view.frame.size.height/2, width: 90, height: 90)
        
        activityView.backgroundColor = UIColor.clear
        activityView.addSubview(actityIndc)
        view.addSubview(activityView)
        
        return activityView
    }
    return nil
}

func removeActivityIndicatorFromSuperView(activity:UIView?){
    if let activity = activity{
        DispatchQueue.main.async {
            activity.removeFromSuperview()
        }
    }
}
