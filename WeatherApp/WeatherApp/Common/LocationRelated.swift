//
//  LocationRelated.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit
import CoreLocation

class LocationRelated:NSObject {
    
    typealias successHandler = (_ coordinate:CLLocationCoordinate2D?) -> Void
    typealias errorHandler = (_ error:String?) -> Void
    // MARK:- Singleton
    static let sharedInstance = LocationRelated()
    
    // MARK:- Private Variable
    private let locationManager = CLLocationManager()
    private var locationHandler:successHandler?
    private var locationError:errorHandler?

//    // MARK:- View Life Cycle
//    override private init() {
//        super.init()
//    }
    
    // MARK:- Variable
    static func isLocationEnabled(success:@escaping (Bool) -> Void) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    success(false)
                case .authorizedAlways, .authorizedWhenInUse:
                    success(true)
                @unknown default:
                    success(false)
            }
        } else {
            success(false)
        }
    }
    
    // MARK:- Methods
    func starLocation(locationHandler : @escaping successHandler ,error:@escaping errorHandler ) {
        self.locationHandler = locationHandler
        self.locationError = error
        locationPermissionCheck()
    }
    
    // MARK:- Private Methods
    private func locationPermissionCheck() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case  .restricted, .denied:
                locationError?(nil)
                showAlertSetting()
            case .authorizedAlways, .authorizedWhenInUse:
                setLocationManager()
            case .notDetermined:
                setLocationManager()
            @unknown default:
                setLocationManager()
            }
        } else {
            locationError?(nil)
            showAlertSetting()
        }
    }
    
    private func setLocationManager() {
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func showAlertSetting() {
        let alert = UIAlertController.init(title: "Alert", message: "Enable the GPS", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: {[weak self] (action) in
            self?.locationError?(nil)
        }))
        alert.addAction(UIAlertAction.init(title: "Settings", style: .default, handler: {[weak self] (action) in
            self?.locationError?(nil)
            if !CLLocationManager.locationServicesEnabled() {
                if let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION") {
                    // If general location settings are disabled then open general location settings
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            } else {
                // Go straight to YOUR app's settings like this. Don't forget to put in your bundle identifier -
                if let bundleId = Bundle.main.bundleIdentifier,
                    let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(bundleId)") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
//                if let url = URL(string: UIApplication.openSettingsURLString) {
//                    // If general location settings are enabled then open location settings for the app
//
//                }
            }
        }))
        showAlertController(alert:alert)
    }
}


// MARK:- CLLocationManagerDelegate
extension LocationRelated: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationHandler?(manager.location?.coordinate)
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationError?((error as NSError).localizedFailureReason)
        manager.stopUpdatingLocation()
    }
}

