//
//  WeatherDetailExt.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

extension WeatherDetail: ReuseableProtocol {
    
    static func insertNew(data:[String:Any]?) -> WeatherDetail? {
        
        var weatherDetail:WeatherDetail?
        if let data = data {
            if let coordinate = getlatAndLong(data: data) {
                if let record = checkIfCategoryIsAlreadyThere(lat: coordinate.lat, long: coordinate.lon) {
                    weatherDetail = setData(data: data, weatherDetail: record)
                } else {
                    let record = NSEntityDescription.insertNewObject(forEntityName: WeatherDetail.reusableIdentifier, into: context) as! WeatherDetail
                    weatherDetail = setData(data: data, weatherDetail: record)
                }
            }
        }
        saveData()
        return weatherDetail
    }
    
    static func checkIfCategoryIsAlreadyThere(lat:String,long:String) -> WeatherDetail? {
        // Create Fetch Request
        
        if let records = getFetchRecord(predicate: nil, classObject: WeatherDetail.reusableIdentifier) as? [WeatherDetail] , records.count > 0 {
            let location = CLLocation.init(latitude: CLLocationDegrees(Double(lat)!), longitude: CLLocationDegrees(Double(long)!))
            let record = records.filter { (weatherDetail) -> Bool in
                let location1 = CLLocation.init(latitude: CLLocationDegrees(Double(weatherDetail.lat!)!), longitude: CLLocationDegrees(Double(weatherDetail.long!)!))
                let distance = location.distance(from: location1)
                if distance < 2000 {
                    return true
                }
                return false
            }
            if record.count > 0 {
                return record[0]
            }
        }
        return nil
    }
    
    static private func setData(data:[String:Any], weatherDetail:WeatherDetail) -> WeatherDetail {
        if let location = data["location"] as? [String:Any] {
            if let lat = location["lat"] as? String , let long = location["lon"] as? String {
                weatherDetail.long = long
                weatherDetail.lat = lat
            }
            if let city = location["name"] as? String {
                weatherDetail.locationName = city
                if let region = location["region"] as? String {
                    weatherDetail.locationName = weatherDetail.locationName! + ", " + region
                }
                if let country = location["country"] as? String {
                    weatherDetail.locationName = weatherDetail.locationName! + ", " + country
                }
            }
            if let localTimeStamp = location["localtime_epoch"] as? Double {
                weatherDetail.localtimeEpoch = localTimeStamp
            }
        }
        if let current = data["current"] as? [String:Any] {
            if let value = current["temperature"] as? Double {
                weatherDetail.temp = value
            }
            if let value = current["wind_speed"] as? Int16 {
                weatherDetail.wind = value
            }
            if let value = current["pressure"] as? Int16 {
                weatherDetail.pressure = value
            }
            if let value = current["precip"] as? Double {
                weatherDetail.percip = value
            }
            if let values = current["weather_descriptions"] as? [String] , values.count > 0  {
                weatherDetail.weatherDesc = values[0]
            } else {
                weatherDetail.weatherDesc = ""
            }
            if let values = current["weather_icons"] as? [String] , values.count > 0  {
                weatherDetail.weatherImgUrl = values[0]
            } else {
                weatherDetail.weatherImgUrl = ""
            }
        }
        return weatherDetail
    }
    
    static private func getlatAndLong(data:[String:Any]?) -> (lat:String,lon:String)? {
        if let data = data , let location = data["location"] as? [String:Any] {
            if let lat = location["lat"] as? String , let long = location["lon"] as? String {
                return (lat:lat,lon:long)
            }
        }
        return nil
    }
}
