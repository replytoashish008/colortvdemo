//
//  WeatherDetail+CoreDataProperties.swift
//  WeatherApp
//
//  Created by Ashish Agrawal on 18/07/20.
//  Copyright © 2020 Ashish Agrawal. All rights reserved.
//
//

import Foundation
import CoreData


extension WeatherDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherDetail> {
        return NSFetchRequest<WeatherDetail>(entityName: "WeatherDetail")
    }

    @NSManaged public var lat: String?
    @NSManaged public var locationName: String?
    @NSManaged public var long: String?
    @NSManaged public var percip: Double
    @NSManaged public var pressure: Int16
    @NSManaged public var temp: Double
    @NSManaged public var weatherImgUrl: String?
    @NSManaged public var weatherDesc: String?
    @NSManaged public var wind: Int16
    @NSManaged public var localtimeEpoch: Double

}
